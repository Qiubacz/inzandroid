package com.example.user.sterowanie;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.net.Socket;


public class Dotyk extends Activity implements View.OnTouchListener, View.OnClickListener {


    private TextView tV1;
    private Integer x = 0, y = 0;
    private static Integer xW, yW, Li2W, Li2S;
    private LinearLayout Li2;
    private Button Bl, Bs, Bp, Be;
    private TcpClient mTcpClient;


    // private String odebranaRozdzielczosc;
    private static String info;

    public static String getInfo() {
        return info;
    }

    public static Integer getxW() {
        return xW;
    }

    public static Integer getyW() {
        return yW;
    }

    public static Integer getL1W() {
        return Li2W;
    }

    public static Integer getLi2S() {
        return Li2S;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dotyk);
        przypisanie();

        Li2.setOnTouchListener(this);
        Bl.setOnClickListener(this);
        Bs.setOnClickListener(this);
        Bp.setOnClickListener(this);
        Be.setOnClickListener(this);
    }

    // po co ta metoda? Dzięki niej czytelnie widzę elementy, kóre przypisuję do tej klasy
    private void przypisanie() {
        tV1 = (TextView) findViewById(R.id.tV9);
        tV1.setText(x + " x " + y);
        Li2 = (LinearLayout) findViewById(R.id.Li2);
        Bl = (Button) findViewById(R.id.bL);
        Bs = (Button) findViewById(R.id.bS);
        Bp = (Button) findViewById(R.id.bP);
        Be = (Button) findViewById(R.id.bW);
    }


    /**
     * Metoda pobierająca ruch po ekranie, do poprawy zczytywanie poza ekranem
     *
     * @param v
     * @param event
     * @return
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Li2W = Li2.getHeight();
        Li2S = Li2.getWidth();

        switch (MotionEventCompat.getActionMasked(event)) {
            case (MotionEvent.ACTION_DOWN):
                //pobieranie danych do wykonania skalowania
                //x
                xW = (int) event.getX();
                //y
                if ((yW = (int) event.getY()) < 0) {
                    yW = 0;
                } else if (yW > Li2W) {
                    yW = Li2W - 5;

                } else {
                    yW = (int) event.getY();
                }

                tV1.setText("x: " + xW + " | y: " + yW);
                //
                info = "cos";
                new ConnectTask().execute("");
                //sends the message to the server
                if (mTcpClient != null) {
                    mTcpClient.sendMessage("");
                }
                return true;

            case (MotionEvent.ACTION_MOVE):
                //pobieranie danych do wykonania skalowania
                xW = (int) event.getX();
                //y
                if ((yW = (int) event.getY()) < 0) {
                    yW = 0;
                } else if (yW > Li2W) {
                    yW = Li2W - 5;

                } else {
                    yW = (int) event.getY();
                }
                tV1.setText("x: " + xW + " | y: " + yW);
                //
                info = "cos";
                new ConnectTask().execute("");
                //sends the message to the server
                if (mTcpClient != null) {
                    mTcpClient.sendMessage("");
                }
                return true;

            case (MotionEvent.ACTION_UP):
                //X
                xW = (int) event.getX();
                //y
                if ((yW = (int) event.getY()) < 0) {
                    yW = 0;
                } else if (yW > Li2W) {
                    yW = Li2W - 5;

                } else {
                    yW = (int) event.getY();
                }
                tV1.setText("x: " + xW + " | y: " + yW);
                info = "cos";
                new ConnectTask().execute("");
                //sends the message to the servers
                if (mTcpClient != null) {
                    mTcpClient.sendMessage("");
                }

                return true;
        }
        return false;
    }

    /**
     * Metoda zczytujaca klik w przyciski
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bL:
                tV1.setText("Wcisnieto srodkowy lewy  ");

                info = "pL";

                new ConnectTask().execute("");
                //sends the message to the server
                if (mTcpClient != null) {
                    mTcpClient.sendMessage("");
                }
                break;
            case R.id.bS:
                tV1.setText("Wcisnieto srodkowy przycisk  ");

                info = "pS";

                new ConnectTask().execute("");
                //sends the message to the server
                if (mTcpClient != null) {
                    mTcpClient.sendMessage("");
                }
                break;
            case R.id.bP:
                tV1.setText("Wcisnieto prawy przycisk");

                info = "pP";

                new ConnectTask().execute("");
                //sends the message to the server
                if (mTcpClient != null) {
                    mTcpClient.sendMessage("");
                }
                break;
            case R.id.bW:
                tV1.setText("Wyłączanie aplikacji");

                info = "pW";

                new ConnectTask().execute("");

                break;
        }

    }

}
