package com.example.user.sterowanie;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.content.Intent;

public class Start extends AppCompatActivity implements View.OnClickListener {
    private TextView tV1;
    private EditText eT1;
    private Button b1;
    private static String adresIP;

    public static String getAdresIP() {
        return adresIP;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        przypisanie();

        b1.setOnClickListener(this);

    }


    private void przypisanie() {
        tV1 = (TextView) findViewById(R.id.tV1);
        eT1 = (EditText) findViewById(R.id.eT1);
        b1 = (Button) findViewById(R.id.b1);

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, Dotyk.class);

        adresIP = eT1.getText().toString();
        if (eT1.equals(null)) {
            tV1.setText("Wporawdź adres IP!");
        } else {
          //  intent.putExtra("IP", adresIP);
            startActivity(intent);

        }


    }
}
