package com.example.user.sterowanie;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Objects;

import android.content.Intent;

import static android.content.Intent.getIntent;
import static android.content.Intent.getIntentOld;
import static android.content.Intent.parseIntent;

/**
 * Created by dgarczarek on 29.10.2017.
 */
//xW x wysyłany, yP y pobrany, xS x ze strumienia, oX obliczone X
public class TcpClient implements Serializable {
    public static String adresIPSerwera;
   // public static  String SERVER_IP = "192.168.43.238"; //server IP address
    public static final int SERVER_PORT = 49153;
    // message to send to the server
    private String mServerMessage;
    // sends message received notifications
    private OnMessageReceived mMessageListener = null;
    // while this is true, the server will continue running
    private boolean mRun = false;
    // used to send messages
    private PrintWriter mBufferOut;
    // used to read messages from the server
    private BufferedReader mBufferIn;
    private long wspolczynnikX2, wspolczynnikY2;
    private String wysilInfo = null;
    private String[] podzielony;
    private Double oX, oY, wspolczynnikX, wspolczynnikY, xS, yS;


    /**
     * Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TcpClient(OnMessageReceived listener) {
        mMessageListener = listener;
    }


    /**
     * Sends the message entered by client to the server
     *
     * @param message text entered by client
     */
    public void sendMessage(String message) {
        if (mBufferOut != null && !mBufferOut.checkError()) {
            mBufferOut.println(message);
            Log.e("sendMessage", "insendMessage");
            mBufferOut.flush();
        }
    }

    /**
     * Close the connection and release the members
     */
    public void stopClient() {

        mRun = false;

        if (mBufferOut != null) {
            mBufferOut.flush();
            mBufferOut.close();
        }

        mMessageListener = null;
        mBufferIn = null;
        mBufferOut = null;
        mServerMessage = null;



    }


    double SkalowanieX(Double xS) {
        wspolczynnikX = (xS / Dotyk.getLi2S());
        wspolczynnikX *= 1000;
        wspolczynnikX2 = Math.round(wspolczynnikX);
        wspolczynnikX = (double) wspolczynnikX2;
        return wspolczynnikX /= 1000;
    }

    double SkalowanieY(Double yS) {
        wspolczynnikY = (yS / Dotyk.getL1W());
        wspolczynnikY *= 1000;
        wspolczynnikY2 = Math.round(wspolczynnikY);
        wspolczynnikY = (double) wspolczynnikY2;
        return wspolczynnikY /= 1000;
    }


    public void run() {


        adresIPSerwera = Start.getAdresIP();

        mRun = true;

        try {
            //here you must put your computer's IP address.
            InetAddress serverAddr = InetAddress.getByName(adresIPSerwera);

            Log.e("TCP Client", "C: Connecting...");

            //create a socket to make the connection with the server
            Socket socket = new Socket(serverAddr, SERVER_PORT);
//
            try {


                //sends the message to the server
                mBufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                //  mBufferOut.print("TestWysylania");

                //receives the message which the server sends back
                mBufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                //odczytanie strumienia
                mServerMessage = mBufferIn.readLine();


                //rozbicie ciągu na dwie części
                podzielony = mServerMessage.split("x");
                //w tym miejscu musi być przeliczenia rozdzielczości

                xS = Double.parseDouble(podzielony[0]);
                yS = Double.parseDouble(podzielony[1]);

                wysilInfo = Dotyk.getInfo();

                if (wysilInfo.equals("pL") || wysilInfo.equals("pS") || wysilInfo.equals("pP") || wysilInfo.equals("pW")) {

                    mBufferOut.println(wysilInfo);
                    wysilInfo = null;
                } else {
                    mBufferOut.println(((Math.round(SkalowanieX(xS) * Dotyk.getxW()) + "x" + Math.round(SkalowanieY(yS) * Dotyk.getyW()))));
                    wysilInfo = null;
                }


                // mBufferOut.println(wysilInfo + "Testowanie wysylania wiadomosci");

                Log.e("Wysłano", "Wyslano" + mBufferOut);
                //call the method messageReceived from MyActivity class
                mMessageListener.messageReceived(mServerMessage); //pobiera rozdzielczosc


                Log.e("RESPONSE FROM SERVER", "S: Received Message: '" + mServerMessage + "'");


            } catch (Exception e) {

                Log.e("TCP", "S: Error", e);

            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close();

            }

        } catch (Exception e) {

            Log.e("TCP", "C: Error", e);

        }


    }

    //Declare the interface. The method messageReceived(String message) will must be implemented in the MyActivity
//class at on asynckTask doInBackground
    public interface OnMessageReceived {
        public void messageReceived(String message);
    }
}
